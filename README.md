# README #

Dashboard para módulo "Controlador"

## Instalación ##

Recomendable crear un virtual env:

`virtualenv controller-env --python=python3`

Activar el virtual env:

`source controller-env/bin/activate`

Descargar o clonar el proyecto y entrar al directorio djcontroller

Instalar dependencias:

`pip install -r requirements.txt`

Ejecutar las migraciones:

`python manage.py migrate`

Probar:

`python manage.py runserver`


## Problemas con django 1.10 ##

Hay un bug en django 1.10, para resolverlo:

 * editar --/lib/python3.4/site-packages/django/core/management/base.py
 * ir a la linea 314 y cambiar `if options['no_color']` por  `if options.get('no_color'):`