from .models import RtControlador, VentasControlador, VolumenVentasControlador
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection
from collections import namedtuple
import random


COLORS = [(139, 0, 0),
          (0, 100, 0),
          (0, 0, 139)]

colors = {'*D': '#88C057',
          '*R': '#FFCC66',
          '*S': '#ED7161'}


def random_color():
    return random.choice(COLORS)


def fetch_volument_ventas():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM volumen_ventas_controlador")
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        for row in cursor.fetchall():
            yield nt_result(*row)


def volumen_de_ventas():
    data = {"min": 0.0,
            "max": 0.0,
            "plot_data": [],
            "labels": ["{0}:00".format(h) for h in range(24)]
            }

    prev_ft = ''
    index = 0

    min_value = 0
    max_value = 0
    for volumen in fetch_volument_ventas():
        if volumen.ft is None:
            break
        if volumen.ft != prev_ft:
            data["plot_data"].append({"label": volumen.ft,
                                      "data": [0,]*24 ,
                                      "backgroundColor": colors[volumen.ft],
                                      "borderColor": colors[volumen.ft],
                                      "strokeColor": colors[volumen.ft],
                                      "fill":False,
                                      })
            #data["colors"].append(colors[volumen.ft])
            prev_ft = volumen.ft
            index = len(data["plot_data"]) - 1

        data["plot_data"][index]["data"][volumen.hour] = volumen.coalesce
            #append([volumen.hour, volumen.coalesce])

        min_value = min(min_value, volumen.coalesce)
        max_value = max(max_value, volumen.coalesce)
    data["min"] = min_value
    data["max"] = max_value
    return data


def fetch_existencia():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM existencia_final_por_ft")
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        for row in cursor.fetchall():
            yield nt_result(*row)


def existencia_final_por_ft():
    data = []

    for ft in fetch_existencia():
        data.append({"ft": ft.ft,
                     "ft.ft_": ft.ft[1],
                     "existencia_final": ft.existencia_final})

    return data


def fetch_totales_de_ventas():
    with connection.cursor() as cursor:
        cursor.execute("SELECT sum(volume) as volume, sum(amount) as amount "
                       "FROM ventas_controlador WHERE nozzle = 1;")
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        row = nt_result(*cursor.fetchone())
        return row


def totales_de_ventas():

    return fetch_totales_de_ventas()


def fetch_page_ventas(start, length, filter):

    queryset = VentasControlador.objects.only("transid", "fp", "ft", "volume",
                                              "amount", "endhours", "tpago",
                                              "nombre_cliente", "codpiloto",
                                              "id_vehiculo")\
                                         .filter(nozzle=1)\
                                         .order_by('-transid').order_by('-tpago')

    total = queryset.count()
    filtered_total = total
    if filter != '':
        queryset = queryset.filter(transid__contains=filter)
        filtered_total = queryset.count()

    paginator = Paginator(queryset, length)  # Shows only 10 records per page
    page = (start/length + 1)
    try:
        ventas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        ventas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        ventas = paginator.page(paginator.num_pages)
    return ventas, total, filtered_total


def fetch_venta_aditivos(start, length, filter=None):
    with connection.cursor() as cursor:
        cursor.execute("SELECT count(*) FROM gobusiness_sessions"
                       " WHERE is_active = TRUE;")
        total = cursor.fetchone()[0]

        cursor.execute("SELECT s.turno_id, s.name, s.product_uom_qty as quantity, "
                       " s.price_unit, s.Price_total FROM sale_order_line as s "
                       " JOIN (SELECT id FROM gobusiness_sessions "
                       " WHERE is_active = TRUE) as P "
                       " ON s.turno_id = P.id;")
        #total = cursor.count()
        filtered_total = total

        return cursor.fetchall()[start: start+length], total, filtered_total
