# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-22 02:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_ventascontrolador'),
    ]

    operations = [
        migrations.CreateModel(
            name='VolumenVentasControlador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hour', models.IntegerField(blank=True, null=True)),
                ('coalesce', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('ft', models.CharField(blank=True, max_length=4, null=True)),
                ('date', models.DateField(blank=True, null=True)),
            ],
            options={
                'managed': False,
                'db_table': 'volumen_ventas_controlador',
            },
        ),
    ]
