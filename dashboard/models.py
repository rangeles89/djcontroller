# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or
# field names.
from __future__ import unicode_literals
from django.db import models

class SaleOrderLine(models.Model):
    create_date = models.DateTimeField(blank=True, null=True)
    qty_to_invoice = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    sequence = models.IntegerField(blank=True, null=True)
    price_unit = models.DecimalField(max_digits=65535, decimal_places=65535)
    product_uom_qty = models.DecimalField(max_digits=65535, decimal_places=65535)
    qty_invoiced = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    #write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    #currency = models.ForeignKey('ResCurrency', models.DO_NOTHING, blank=True, null=True)
    #create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    price_tax = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    #product_uom = models.ForeignKey('ProductUom', models.DO_NOTHING, db_column='product_uom')
    customer_lead = models.FloatField()
    #company = models.ForeignKey('ResCompany', models.DO_NOTHING, blank=True, null=True)
    name = models.TextField()
    state = models.TextField(blank=True, null=True)
    #order_partner = models.ForeignKey('ResPartner', models.DO_NOTHING, blank=True, null=True)
    #order = models.ForeignKey('SaleOrder', models.DO_NOTHING)
    price_subtotal = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    discount = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    price_reduce = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    qty_delivered = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    price_total = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    invoice_status = models.TextField(null=True)
    #product = models.ForeignKey('ProductProduct', models.DO_NOTHING)
    #salesman = models.ForeignKey('ResUsers', models.DO_NOTHING, blank=True, null=True)
    #product_packaging = models.ForeignKey('ProductPackaging', models.DO_NOTHING, db_column='product_packaging', blank=True, null=True)
    #route = models.ForeignKey('StockLocationRoute', models.DO_NOTHING, blank=True, null=True)
    barcode = models.TextField(blank=True, null=True)
    subtotal = models.FloatField(blank=True, null=True)
    turno_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sale_order_line'


class GobusinessSessions(models.Model):
    #create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    #user = models.ForeignKey('ResUsers', models.DO_NOTHING, blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)
    #write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    date_init = models.DateTimeField(blank=True, null=True)
    is_active = models.NullBooleanField()
    endday = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gobusiness_sessions'


class AccountPaymentTerm(models.Model):
    #create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    create_date = models.DateTimeField(blank=True, null=True)
    name = models.TextField()
    #company = models.ForeignKey('ResCompany', models.DO_NOTHING)
    #write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    active = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'account_payment_term'

    def __str__(self):
        return self.name


class VentasControlador(models.Model):
    create_date = models.DateTimeField(blank=True, null=True)
    omni = models.CharField(max_length=2, blank=True, null=True)
    endday_moved0 = models.CharField(max_length=10, blank=True, null=True)
    nozzle = models.IntegerField(blank=True, null=True)
    ppu = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    transid = models.CharField(unique=True, max_length=8, blank=True, null=True)
    authpos = models.CharField(max_length=2, blank=True, null=True)
    transactionstatus = models.TextField(blank=True, null=True)
    closedby = models.IntegerField(blank=True, null=True)
    fp = models.CharField(max_length=2, blank=True, null=True)
    ft = models.CharField(max_length=4, blank=True, null=True)
    volume = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    pstat = models.CharField(max_length=2, blank=True, null=True)
    endhours = models.CharField(max_length=10, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    attendant = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    tstat = models.CharField(max_length=2, blank=True, null=True)
    amount = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    endday_moved1 = models.DateField(blank=True, null=True)
    endday = models.TextField(blank=True, null=True)
    #sesion = models.ForeignKey(
    #    'GobusinessSessions', models.DO_NOTHING, blank=True, null=True)
    # Field name made lowercase.
    id_vehiculo_moved0 = models.IntegerField(
        db_column='Id_Vehiculo_moved0', blank=True, null=True)
    # Field name made lowercase.
    cod_empleado = models.IntegerField(
        db_column='Cod_Empleado', blank=True, null=True)
    # Field name made lowercase.
    km_vehiculo = models.TextField(
        db_column='Km_Vehiculo', blank=True, null=True)
    # Field name made lowercase.
    nombre_cliente = models.TextField(
        db_column='Nombre_Cliente', blank=True, null=True)
    # Field name made lowercase.
    codpiloto_moved0 = models.IntegerField(
        db_column='CodPiloto_moved0', blank=True, null=True)
    # Field name made lowercase.
    codcliente = models.TextField(db_column='CodCliente', blank=True, null=True)
    # Field name made lowercase.
    tpago = models.ForeignKey(
        'AccountPaymentTerm', models.DO_NOTHING, db_column='TPago', blank=True,
        null=True)
    #details = models.ForeignKey(
    #    'GobusinessDetailsSale', models.DO_NOTHING, blank=True, null=True)
    existencia = models.FloatField(blank=True, null=True)
    saldo = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    utilidad = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    costo = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    # Field name made lowercase.
    id_vehiculo = models.TextField(
        db_column='Id_Vehiculo', blank=True, null=True)
    # Field name made lowercase.
    codpiloto = models.TextField(
        db_column='CodPiloto', blank=True, null=True)
    existencia_inicial = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    valor_inventario = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    existencia_final = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)
    precio_promedio = models.DecimalField(
        max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ventas_controlador'


class RtControlador(models.Model):
    TSTAT_LIST=(
                ('I','Inactivo'),
                ('A','Venta Autorizada'),
                ('B','Ocupado'),
                ('C','Transacción Completada'),
                ('R','Transacción obtenida'),
                ('D','Campo de datos'),
                ('H','Ocupado'),
                ('Z','Autorizado por POS'),
                ('S','Detenido por POS'),
                ('X','Desconectado'),
                )


    PSTAT_LIST=(
                ('i','Inactivo'),
                ('u','Pistola descolgada'),
                ('a','Venta autorizada'),
                ('p','Despachando combustible'),
                ('t','Venta terminada'),
                ('s','Detenido por POS'),
                ('x','Desconectado'),
                ('k','Keypad Preset Pending'),
                )

    FT_LIST = (
                ('*R', 'Regular'),
                ('*D', 'Diesel'),
                ('*S', 'Súper')
                )
    create_date = models.DateTimeField(blank=True, null=True)
    omni = models.CharField(max_length=2, blank=True, null=True)
    endday_moved0 = models.CharField(max_length=10, blank=True, null=True)
    nozzle = models.IntegerField(blank=True, null=True)
    #write_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='write_uid', blank=True, null=True)
    ppu = models.DecimalField(max_digits=65535, decimal_places=65535,
                              blank=True, null=True)
    transid = models.CharField(max_length=8, blank=True, null=True)
    authpos = models.CharField(max_length=2, blank=True, null=True)
    transactionstatus = models.TextField(blank=True, null=True)
    #create_uid = models.ForeignKey('ResUsers', models.DO_NOTHING, db_column='create_uid', blank=True, null=True)
    closedby = models.IntegerField(blank=True, null=True)
    fp = models.CharField(max_length=2, blank=True, null=True)
    ft = models.CharField(max_length=4, blank=True, null=True, choices=FT_LIST)
    volume = models.DecimalField(max_digits=65535, decimal_places=65535,
                                 blank=True, null=True)
    pstat = models.CharField(max_length=2, blank=True, null=True, choices=PSTAT_LIST)
    endhours = models.CharField(max_length=10, blank=True, null=True)
    write_date = models.DateTimeField(blank=True, null=True)
    attendant = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    tstat = models.CharField(max_length=2, blank=True, null=True, choices=TSTAT_LIST)
    amount = models.DecimalField(max_digits=65535, decimal_places=65535,
                                 blank=True, null=True)
    endday_moved1 = models.DateField(blank=True, null=True)
    endday = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rt_controlador'


class VolumenVentasControlador(models.Model):
    hour = models.IntegerField(blank=True, null=True)
    coalesce = models.DecimalField(max_digits=65535, decimal_places=65535,
                                   blank=True, null=True)
    ft = models.CharField(max_length=4, blank=True, null=True)
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'volumen_ventas_controlador'


INTERVAL_TXT = "Intervalo de actualización de {0}."


class DashboardSettings(models.Model):
    fp_refresh_interval = models.IntegerField(default=5,
                                              verbose_name=INTERVAL_TXT.
                                              format('Fuel Points'))
    ft_char_refresh_interval = models.IntegerField(default=10,
                                                   verbose_name=INTERVAL_TXT.
                                                   format('Gráfica de ventas de producto'))
    exboxes_refresh_interval = models.IntegerField(default=10,
                                                   verbose_name=INTERVAL_TXT.
                                                   format('Cuadros de existencia'))
    totales_refresh_interval = models.IntegerField(default=15,
                                                   verbose_name=INTERVAL_TXT.
                                                   format('Cuadros de totales'))
    tabletxns_refresh_interval = models.IntegerField(default=20,
                                                     verbose_name=INTERVAL_TXT.
                                                     format('Tabla de detalles de ventas'))
    tablelubs_refresh_interval = models.IntegerField(default=20,
                                                     verbose_name=INTERVAL_TXT.
                                                     format('Tabla ventas de lubricantes'))

    class Meta:
        verbose_name = "Configuración de Dashboard"

    def __str__(self):
        return "Configuración"

    def has_add_permission(self, request):
        # if there's already an entry, do not allow adding
        count = DashboardSettings.objects.all().count()
        if count == 0:
            return True
        return False
