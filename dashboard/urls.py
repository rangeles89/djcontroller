from django.conf.urls import url
from django.conf.urls.static import static

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^get_volumen_de_ventas/$',
        views.get_volumen_de_ventas, name='get_volumen_de_ventas'),
    url(r'^get_existencia_final/$',
        views.get_existencia_final, name='get_existencia_final'),
    url(r'^get_ventas/$', views.get_ventas, name='get_ventas'),
    url(r'^get_ventas_aditivos/$', views.get_ventas_aditivos,
        name='get_ventas_aditivos'),
    url(r'^get_controllers_data/$', views.get_controllers_data,
        name='get_controllers_data'),
    url(r'^get_totales_data/$', views.get_totales_data,
        name='get_totales_data'),
    url(r'^get_existencias_data/$', views.get_existencias_data,
        name='get_existencias_data')

    ]
