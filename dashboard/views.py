from django.shortcuts import render
from django.http import (HttpResponse, HttpResponseRedirect,
                         JsonResponse, HttpResponseBadRequest)
from django.shortcuts import render_to_response, redirect, render
from django.template.loader import render_to_string
from django.template import RequestContext
from .models import RtControlador, DashboardSettings
from .controller import (volumen_de_ventas, existencia_final_por_ft,
                         totales_de_ventas, fetch_page_ventas,
                         fetch_venta_aditivos)


def get_volumen_de_ventas(request):
    json_response = []
    json_response.append(volumen_de_ventas())
    return JsonResponse(json_response, safe=False)


def get_existencia_final(request):
    json_response = existencia_final_por_ft()
    return JsonResponse(json_response, safe=False)


def get_controllers_data(request):
    controllers = RtControlador.objects.all().order_by("fp")
    c = {'controllers': controllers}
    return render(request, "groupcontrollers.html", c)


def get_totales_data(request):
    c = {'totales_de_ventas': totales_de_ventas()}
    return render(request, "totales.html", c)


def get_existencias_data(request):
    c = {'existencia_final': existencia_final_por_ft()}
    return render(request, "existencias.html", c)


def index(request):
    controllers = RtControlador.objects.all().order_by("fp")
    settings = DashboardSettings.objects.first()

    c = {'controllers': controllers,
         'existencia_final': existencia_final_por_ft(),
         'totales_de_ventas': totales_de_ventas(),
         'settings': {"fp_refresh_interval": settings.fp_refresh_interval*1000,
                      "exboxes_refresh_interval": settings.exboxes_refresh_interval*1000,
                      "totales_refresh_interval": settings.totales_refresh_interval*1000,
                      "tabletxns_refresh_interval": settings.tabletxns_refresh_interval*1000,
                      "tablelubs_refresh_interval": settings.tablelubs_refresh_interval*1000,
                      "ft_char_refresh_interval": settings.ft_char_refresh_interval*1000
                      }
         }

    return render(request, "dashboard.html", c)


def get_ventas(request):
    start = int(request.GET['start'])
    page_size = int(request.GET['length'])
    page_filter = request.GET.get('search')
    page_filter = request.GET.get('search[value]')
    print(page_filter)

    page, total, filtered = fetch_page_ventas(start, page_size, page_filter)
    json_response = {}
    json_response["recordsTotal"] = total
    json_response["recordsFiltered"] = filtered
    json_response["data"] = [[p.transid, p.fp, p.ft, p.volume,
                              p.endhours, str(p.tpago),
                              p.codpiloto,
                              p.id_vehiculo] for p in page]
    print(json_response)
    return JsonResponse(json_response, safe=False)


def get_ventas_aditivos(request):
    start = int(request.GET.get('start', 0))
    page_size = int(request.GET.get('length', 0))
    page_filter = request.GET.get('search[value]')
    if page_filter is None:
        page_filter = ''
    print(page_filter)

    page, total, filtered = fetch_venta_aditivos(start, page_size, page_filter)
    json_response = {}
    json_response["recordsTotal"] = total
    json_response["recordsFiltered"] = filtered
    json_response["data"] = [[p[0], p[1], p[2],
                              "Q%0.2f" % float(p[3]),
                              "Q%0.2f" % float(p[4])] for p in page]
    print(json_response)
    return JsonResponse(json_response, safe=False)
