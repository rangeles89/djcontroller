from django import template
from django.contrib.humanize.templatetags.humanize import intcomma


register = template.Library()


@register.filter
def prepend_dollars(dollars):
    if dollars:
        dollars = round(float(dollars), 2)
        return "Q%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])
    else:
        return "Q0.00"


@register.filter
def fpcolor(value):
    colors = {'I': 'red', 'A': 'green', 'C': 'cyan'}
    return colors[value]

TSTAT_COLORS={
              'I':'#999999',
              'A':'#48A0DC',
              'B':'#FF0000',
              'C':'#9CD43A',
              'R':'#9CD43A',
              'D':'#FF0000',
              'H':'#FF0000',
              'Z':'#FF0000',
              'S':'#FF0000',
              'X':'#FF0000',
              }


PSTAT_COLORS={
                'i':'#999999',
                'u':'#AEFFFF',
                'a':'#48A0DC',
                'p':'#cc3366',
                't':'#9CD43A',
                's':'#FF0000',
                'x':'#FF0000',
                'k':'#FF8080',
                }

@register.filter
def tstatcolor(value):
    print("tstatcolor:"+value)
    return TSTAT_COLORS.get(value,'#AAAAAA')


@register.filter
def pstatcolor(value):
    print("pstatcolor:"+value)
    return PSTAT_COLORS.get(value,'#AAAAAA')


@register.filter
def ftcolor(value):
    """
    *D=#88C057
    *R=#FFCC66
    *S= #ED7161
    """
    colors = {'*D': '#88C057',
              '*R': '#FFCC66',
              '*S': '#ED7161'}
    return colors.get(value,'#AAAAAA')
