function load_volume_chart(data){

    var plot_data = {labels: data.labels,
                    datasets: data.plot_data, };

    var plot_settings = {
        type: 'line',
        scales: {
            yAxes: [{
                ticks: {
                    max: data.max,
                    min: data.min,

                }
            }],

        }
    };

    if ($("#chart_volume").length){
        console.log('Plot2');

        /*$.plot( $("#chart_volume"),
        plot_data,
        plot_settings);*/
        var canvas = document.getElementById("cchart_volume");
        var ctx = canvas.getContext('2d');
        var myBarChart = new Chart(ctx, {
            type: 'line',
            data: plot_data,
            //options: plot_settings
        });
    }
}
