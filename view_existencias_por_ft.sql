-- View: existencia_final_por_ft

-- DROP VIEW existencia_final_por_ft;

CREATE OR REPLACE VIEW existencia_final_por_ft AS 
 SELECT v.transid,
    v.ft,
    v.existencia_final
   FROM ventas_controlador v,
    ( SELECT max(ventas_controlador.transid::text) AS transid,
            ventas_controlador.ft
           FROM ventas_controlador
          WHERE ventas_controlador.nozzle >= 0 AND ventas_controlador.pstat::text = 'p'::text
          GROUP BY ventas_controlador.ft) p
  WHERE v.transid::text = p.transid;


