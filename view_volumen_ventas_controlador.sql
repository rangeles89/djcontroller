CREATE OR REPLACE VIEW volumen_ventas_controlador AS 
 SELECT s.hour,
    COALESCE(t.volume, 0::numeric) AS "coalesce",
    t.ft,
    t.date
   FROM generate_series(0, 23) s(hour)
     LEFT JOIN ( SELECT ventas_controlador.ft,
            sum(ventas_controlador.volume) AS volume,
            date_part('hour'::text, ventas_controlador.create_date) AS hour,
            date(ventas_controlador.create_date) AS date,
            sum(ventas_controlador.amount) AS sum
           FROM ventas_controlador
          WHERE ventas_controlador.nozzle = 1
          GROUP BY date(ventas_controlador.create_date), ventas_controlador.ft, date_part('hour'::text, ventas_controlador.create_date)) t ON t.hour = 
s.hour::double precision
  ORDER BY t.ft, s.hour;

